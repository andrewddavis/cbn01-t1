#!/bin/bash

# Process T1 anatomical with FSL's fsl_anat tool
#
# This script is specific to CAN-BIND _A Brains (esp. fslcreatehd part...). It
#   should be run from within an exam directory.
#
# Checks to make sure the anatomicals we're dealing with are the expected ones
#   must be done in advance.

# For a guide to using the FSL anatomical tools (mostly
#   FAST, for our purposes), see:
#   http://fsl.fmrib.ox.ac.uk/fslcourse/lectures/practicals/seg_struc/
# For a guide to fsl_anat, see:
#   http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat

# v0.3 (May 2015) by Andrew Davis (addavis@gmail.com)

printUsage () {
cat << EOF

   $(basename $0)
   ------------

   This script runs fsl_anat on an anatomical to get a brain-extracted
     target for registration.  Some stats are written out to
     t1_anatomical.anat/T1_vols.txt.

   Usage: $(basename $0) [anat_nii]

     anat_nii :  NIfTI to process.  Default "t1_anatomical".

EOF
}

# Robust options
set -o nounset    # fail on unset variables
set -o errexit    # fail on non-zero return values
set -o pipefail   # fail if any piped command fails

# Defaults and parse args
if [[ $# -eq 0 ]]; then
    anat_nii=t1_anatomical
elif [[ -n "${1:-}" ]] && [[ "$1" = "-h" || "$1" = "-help" || "$1" = "--help" ]]; then
    printUsage
    exit 0
elif [[ -n "${1:-}" ]] && [[ -e "$1" || -e "$1".nii || -e "$1".nii.gz ]]; then
    anat_nii=$("$FSLDIR"/bin/remove_ext "$1")
else
    echo "Error: file not found: $1"
    exit 2
fi

# Check for expected tools
if ! which fsl > /dev/null; then
    echo "Error: FSL not found on path."
    exit 2
elif [[ -z ${FSLDIR:-} ]]; then
    echo "Error: FSLDIR not set."
    exit 2
elif ! which pycalc > /dev/null; then
    echo "Error: pycalc not found on path."
    exit 2
fi

# Bow out if it appears to already have been run
[[ -d "${anat_nii}".anat ]] \
  && { echo "Warning: ${anat_nii}.anat exists in $(basename $PWD). Skipping..."; exit 0; }

echo "Processing ${anat_nii} in $(basename $PWD)..."

if [[ $(fslval "${anat_nii}" dim1) -eq 512 ]]; then
    # Downsample artificial 512x512 image to 220x220 for MCM acquired images
    #  This solves (most of) the problem of partial volume artifacts 'inventing' grey matter
    #  Create a template with correct in-plane matrix, then use flirt to transform our image
    #    with the identity transform
    # Usage: fslcreatehd <xsize> <ysize> <zsize> <tsize> <xvoxsize> <yvoxsize> <zvoxsize> <tr> <xorigin> <yorigin> <zorigin> <datatype> <headername>
    p=($(fslval "${anat_nii}" dim1)
       $(fslval "${anat_nii}" dim2)
       $(fslval "${anat_nii}" dim3)
       $(fslval "${anat_nii}" dim4)
       $(fslval "${anat_nii}" pixdim1)
       $(fslval "${anat_nii}" pixdim2)
       $(fslval "${anat_nii}" pixdim3)
       $(fslval "${anat_nii}" pixdim4)
       $(fslval "${anat_nii}" datatype))
    fslcreatehd 220 220 ${p[2]} 1 1.0 1.0 1.0 ${p[7]} 0 0 0 ${p[8]} "${anat_nii}"-220_ref

    echo "Downsampling ${anat_nii} (back) to 220x220..."
    "${FSLDIR}"/bin/immv "${anat_nii}" "${anat_nii}"-orig_512
    flirt -in "${anat_nii}"-orig_512 -out "${anat_nii}" -ref "${anat_nii}"-220_ref \
          -applyxfm -init "$FSLDIR"/etc/flirtsch/ident.mat -interp sinc
    "$FSLDIR"/bin/imrm "${anat_nii}"-220_ref
fi

# Use fsl_anat script to do the main work
#  --weakbias option uses params to FAST: niter=10, smooth=20
nice fsl_anat -t T1 --weakbias --nosubcortseg -i "${anat_nii}"

# Grab the main file of interest and some stats
echo "Moving files and compiling stats..."
"$FSLDIR"/bin/immv "${anat_nii}".anat/T1_biascorr "${anat_nii}"_biascorr
"$FSLDIR"/bin/immv "${anat_nii}".anat/T1_biascorr_brain "${anat_nii}"_biascorr_brain
"$FSLDIR"/bin/immv "${anat_nii}".anat/T1_biascorr_brain_mask "${anat_nii}"_biascorr_mask
"$FSLDIR"/bin/imrm "${anat_nii}".anat/T1
"$FSLDIR"/bin/imrm "${anat_nii}".anat/T1_fullfov
"$FSLDIR"/bin/imrm "${anat_nii}".anat/T1_orig

# GM
stat_str=$(fslstats "${anat_nii}".anat/T1_fast_pve_1 -M -V)
IFS=' ' read -ra gm_stats <<< "$stat_str"
gm_vol_cm3=$(pycalc "${gm_stats[0]}*${gm_stats[2]}/1000")

# WM
stat_str=$(fslstats "${anat_nii}".anat/T1_fast_pve_2 -M -V)
IFS=' ' read -ra wm_stats <<< "$stat_str"
wm_vol_cm3=$(pycalc "${wm_stats[0]}*${wm_stats[2]}/1000")

# Total brain
tot_vol_cm3=$(pycalc "$gm_vol_cm3 + $wm_vol_cm3")

# Write stats to log file
printf "\nVolumes from FAST's PVE segmentation:\n" >> "${anat_nii}".anat/T1_vols.txt
printf "GM: %0.0f cm^3\n" $gm_vol_cm3 | tee -a "${anat_nii}".anat/T1_vols.txt
printf "WM: %0.0f cm^3\n" $wm_vol_cm3 | tee -a "${anat_nii}".anat/T1_vols.txt
printf "Total: %0.0f cm^3\n" $tot_vol_cm3 | tee -a "${anat_nii}".anat/T1_vols.txt

echo ""
exit 0
